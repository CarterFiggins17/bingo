import sys

import NumberSet    # Not sure if i need this. I get it form Deck

class Card():
    def __init__(self, idnum, size, numberSet):
        self.idnum = idnum
        self.size = size
        self.numberSet = numberSet


    def getId(self):  # All Done
        """Return an integer: the ID number of the card"""
        return self.idnum

    def getSize(self): # All Done
        """Return an integer: the size of one dimension of the card.
        A 3x3 card will return 3, a 5x5 card will return 5, etc.
        """
        return self.size

    def print(self, file=sys.stdout):    # ******* NEED TO FIND OUT HOW TO PRINT TO FILE *************.
                                         # ALSO NEED TO UPDATE UML
        """void function:
        Prints a card to the screen or to an open file object"""
        freeSpot = (self.size - (int)(self.size/2))
        odd = False
        if(self.size % 2 == 1):
            odd = True
        # print(f"Card {self.idnum + 1}")
        file.write(f"Card {self.idnum + 1}\n")
        count = 0
        for i in range(0, self.size):
            myString = ""
            # print(("+-----"*self.size) + "+")
            file.write(("+-----"*self.size) + "+\n")
            for j in range(0, self.size):
                if(i == freeSpot -1 and j == freeSpot-1 and odd):
                    myString += f"|FREE!"
                else:
                    myString += f"|{'{:^5}'.format(self.numberSet.getNext())}"
                count+= 1
            myString += "|\n"
            # print(myString)
            file.write(myString)
        # print(("+-----" * self.size) + "+")
        file.write(("+-----" * self.size) + "+\n")
        self.numberSet.count = 0





