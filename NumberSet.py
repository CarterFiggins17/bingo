import random

class NumberSet():
    def __init__(self, size):
        self.size = size
        self.numList = []
        self.count = 0
        for i in range(0, size):
            self.numList.append(i+1)

    def getSize(self):
        """Return an integer: the size of the NumberSet"""
        return self.size

    def get(self, index):
        """Return an integer: get the number from this NumberSet at an index"""
        if self.size == 0:
            return None
        if index >= 0 and index <= self.size:
            return self.numList[index]


    def randomize(self):
        """void function: Shuffle this NumberSet"""
        random.shuffle(self.numList)
        self.numList.reverse()

    def getNext(self):

        if self.count is len(self.numList):
            return None
        else:
            number = self.numList[self.count]
            self.count += 1
            return number