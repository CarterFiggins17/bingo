import Deck
import Menu

class UserInterface():
    def __init__(self):
        self.__m_currentDeck = None



    def run(self):
        """Present the main menu to the user and repeatedly prompt for a valid command"""
        print("Welcome to the Bingo! Deck Generator\n")
        menu = Menu.Menu("Main")
        menu.addOption("C", "Create a new deck")
        
        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "C":
                self.__createDeck()
            elif command == "X":
                keepGoing = False


    def __createDeck(self):
        """Command to create a new Deck"""
        # TODO: Get the user to specify the card size, max number, and number of cards
        loop = True
        while loop:
            cardSize = input("Size of Card From 3 to 15: ")
            min = 2 * pow((int)(cardSize), 2)
            max = 2 * pow((int)(cardSize), 4)
            if (int)(cardSize) <= 15 and (int)(cardSize) >= 3:
                loop = False
            else:
                print("Number invalid")
        loop = True
        while loop:
            numberMax = input(f"Pick Highest number from {min} to {max}: ")  # What numbers do I need?
            if (int)(numberMax) <= max  and (int)(numberMax) >= min:
                loop = False
            else:
                print("Number invalid")
        loop = True
        while loop:
            cardCount = input("Number of Cards from 3 to 10000: ")
            if (int)(cardCount) <= 10000  and (int)(cardCount) >= 3:
                loop = False
            else:
                print("Number invalid")



        # TODO: Create a new deck
        self.__m_currentDeck = Deck.Deck((int)(cardSize), (int)(cardCount), (int)(numberMax))
        # TODO: Display a deck menu and allow use to do things with the deck
        self.__deckMenu()


    def __deckMenu(self):
        """Present the deck menu to user until a valid selection is chosen"""
        menu = Menu.Menu("Deck")
        menu.addOption("P", "Print a card to the screen");
        menu.addOption("D", "Display the whole deck to the screen");
        menu.addOption("S", "Save the whole deck to a file");
        menu.addOption("N", "Deletes deck and creates new deck")

        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "P":
                self.__printCard()
            elif command == "D":
                print()
                self.__m_currentDeck.print()
            elif command == "S":
                self.__saveDeck()
            elif command == "N":
                self.__m_currentDeck = None
                self.__createDeck()
                keepGoing = False
            elif command == "X":
                keepGoing = False


    def __printCard(self):   # Not Working
        """Command to print a single card"""
        cardToPrint = self.__getNumberInput("Id of card to print", 1, self.__m_currentDeck.getCardCount())
        if (int)(cardToPrint) > 0:
            print()
            self.__m_currentDeck.print(idx=(int)(cardToPrint))


    def __saveDeck(self):    # NEED WORK
        """Command to save a deck to a file"""
        fileName = self.__getStringInput("Enter output file name")
        if fileName != "":
            # TODO: open a file and pass to currentDeck.print()
            outputStream = open(fileName, 'w')
            self.__m_currentDeck.print(outputStream)
            outputStream.close()
            print("Done!")

    def __getNumberInput(self, prompt, index, cardNumbers):
        keepGoing = True
        while keepGoing:
            cardNumber = input(f"{prompt} from 1 to {cardNumbers}")
            if((int)(cardNumber) >= 1 and (int)(cardNumber) <= cardNumbers):
                return cardNumber
                keepGoing = False
            else:
                print("Not valid number Try again")

    def __getStringInput(self, prompt):
        return input(prompt)